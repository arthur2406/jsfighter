import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {

  const bodyElement = createElement({
    tagName: 'div',
    className: 'modal-body',
  });
  const image = createFighterImage(fighter);
  bodyElement.appendChild(image);
  const title = `${fighter.name} WON!`;
  const onClose = () => location.reload();

  showModal({ title, bodyElement, onClose });
}
