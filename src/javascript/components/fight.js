import { controls } from '../../constants/controls';


class FighterState {
  constructor(fighter, position, combination) {
    this.description = fighter;
    this.currentHealth = fighter.health;
    this.hb = document.getElementById(`${position}-fighter-indicator`);
    this.isBlocking = false;
    this.canHitCritical = true;
    this.combination = combination;
  };

  updateHb() {
    const healthInPercents = this.currentHealth * 100 / this.description.health;
    this.hb.style.width = `${healthInPercents}%`; 
  };
};

export async function fight(firstFighter, secondFighter,) {
  return new Promise((resolve) => {

    const fState1 = new FighterState(firstFighter, 'left',
      controls.PlayerOneCriticalHitCombination);

    const fState2 = new FighterState(secondFighter, 'right', 
      controls.PlayerTwoCriticalHitCombination);
      
    const pressedKeys = new Set();
    
    const combinations = [
      fState1.combination,
      fState2.combination
    ];

    const checkCombination = () => {
      combinations.forEach((combination, i) => {
        if (combination.every(k => pressedKeys.has(k))) {
          i === 0 ? hitCritical(fState1, fState2) : hitCritical(fState2, fState1);
        } 
      });
    };


    const checkWinner = () => {
      fState1.currentHealth <= 0 ? resolve(secondFighter) : (fState2.currentHealth <= 0 && resolve(firstFighter));  
    };

    document.addEventListener('keydown', e => {
      pressedKeys.add(e.code);
      checkCombination();
      
      switch (e.code) {
        case controls.PlayerOneAttack: 
          hit(fState1, fState2);
          break;
        case controls.PlayerTwoAttack:
          hit(fState2, fState1);
          break;
        case controls.PlayerOneBlock:
          startBlocking(fState1);
          break;
        case controls.PlayerTwoBlock:
          startBlocking(fState2);
          break;

      }
      
      checkWinner();
    });

    document.addEventListener('keyup', e => {
      pressedKeys.delete(e.code);
      switch (e.code) {
        case controls.PlayerOneBlock:
          stopBlocking(fState1);
          break;
        case controls.PlayerTwoBlock:
          stopBlocking(fState2);
          break;
      }
    });
  });
}

export function startBlocking(fState) {
  fState.isBlocking = true;
};

export function stopBlocking(fState) {
  fState.isBlocking = false;
}


export function hit(attacker, defender) {
  if (attacker.isBlocking || defender.isBlocking) return;
  const damage = getDamage(attacker, defender);
  defender.currentHealth -= damage;
  defender.updateHb();
};

export function hitCritical(attacker, defender) {
  if (attacker.isBlocking || !attacker.canHitCritical) return;
  defender.currentHealth -= attacker.description.attack * 2;
  defender.updateHb();
  attacker.canHitCritical = false;
  setTimeout((() => attacker.canHitCritical = true), 10000);
};


export function getDamage(attacker, defender) {
  const hit = getHitPower(attacker.description.attack);
  const block = getBlockPower(defender.description.defense);
  if (block > hit) {
    return 0;
  }
  const damage = hit - block;
  return damage;
}

export function getHitPower(attack) {
  
  const criticalHitChance = Math.random() + 1;
  return attack * criticalHitChance;
}

export function getBlockPower(defense) {
  const dodgeChance = Math.random() + 1;
  return defense * dodgeChance;
}

