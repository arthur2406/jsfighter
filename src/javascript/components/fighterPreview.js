import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  if (!fighter) return;
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const descriptionElement = createElement({
    tagName: 'div',
    className: 'fighter-preview__description'
  });

  const { name, health, attack, defense } = fighter;
  const props = { name, health, attack, defense };

  Object.keys(props).forEach(key => {
    const field = createElement({
      tagName: 'p',
      className: 'fighter-preview__field'
    });

    const t = document.createTextNode(`${key}: ${props[key]}`);
    field.appendChild(t);
    descriptionElement.appendChild(field); 
  });

  const image = createFighterImage(fighter);

  fighterElement.appendChild(descriptionElement);
  fighterElement.appendChild(image);
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
